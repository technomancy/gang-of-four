# Gang of Four

A 4-player [card game](http://www.thegamesjournal.com/reviews/GangOfFour.shtml).

Written with kids.

## Usage

By default it will start a game with 3 computer players:

    $ lua go4.lua

The computer can play against itself without any humans:

    $ lua go4.lua computer computer computer computer

You can override the type of the players by passing in other arguments:

    $ lua go4.lua local computer computer remote:8425

For multiplayer, add a player type of "remote" followed by a colon and
a port number. This will start a server on that port for the player to
connect to from another server. This is the recommended way to connect:

    $ rlwrap nc server.local 8425

Of course, you'll need to replace "server.local" with the hostname of
the server computer and put in the same port you used when you started
the game. If you want to have multiple remote players, they'll need to
use different port numbers.

Requires "luasocket" for multiplayer games:

    $ luarocks install --local luasocket

## Minecraft

It should work to play this game in
[ComputerCraft](http://computercraft.info/) computers inside
MineCraft. You'll need to use the HTTP API to pull in the text of the
game inside a Lua session:

```lua
code = http.get("http://p.hagelb.org/go4.lua").readAll()
f = io.open("go4", "w")
f.write(code)
f:close()
```

Then run `go4`, but you may have trouble with text scrolling off the
screen; ComputerCraft computers do not implement scrollback
unfortunately.

## Ideas for Future

* Graphics with LÖVE
* Multiplayer inside Minecraft
* Minetest compatibility

Copyright © 2016 Phil and Noah and Zach Hagelberg
