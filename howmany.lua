arg = nil                       -- prevent it from starting the game
require("go4")

math.randomseed(tonumber(os.time()))

function has_gang_of(size, hand)
   local same = 1
   local how_far_to_check = 15
   -- gang of seven must be in the first 7 cards
   if(size == 7) then how_far_to_check = 6 end
   for i=1,how_far_to_check do
      if(math.floor(hand[i]) == math.floor(hand[i+1])) then
         same = same + 1
      else
         same = 1
      end
      if(same == size) then return true end
   end
end

function look_for_gang(gang_size, tried)
   local hands = lume.map(deal(make_deck()), lume.sort)
   for i=1,4 do
      local hand = hands[i]
      if(has_gang_of(gang_size, hand)) then
         show_hand(hand)
         print("WE FOUND A GANG OF " .. gang_size .."! It took us " .. tried .. " times.")
         return tried
      else
         return look_for_gang(gang_size, tried + 1)
      end
   end
end

local gang_size = tonumber(arg and arg[1] or 7)
look_for_gang(gang_size, 0)
